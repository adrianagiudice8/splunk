package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientesController {

	Logger logger = LoggerFactory.getLogger(ClientesController.class);

	//tipos de log
	@RequestMapping("/logs")
	public String insex() {
		logger.trace("A TRACE Message");
		logger.debug("A DEBUG Message");
		logger.info("An INFO Message");
		logger.warn("A WARN Message");
		logger.error("An ERROR Message");
		return "Howdy! Check out the Logs to see the output...";
	}

	//executa como http://localhost:8080/logs-aprovado no browse e depois coloca na busca do splunk
	@RequestMapping("/logs-aprovado")
	public String aprovado() {
		logger.trace("cartao Aprovado");
		return "aprovado...";

	}
	//executa como http://localhost:8080/logs-reprovado no browse e depois coloca na busca do splunk

	@RequestMapping("/logs-reprovado")
	public String reprovado() {
		logger.trace("cartao Reprovado");
		return "Reprovado...";
	}

}
